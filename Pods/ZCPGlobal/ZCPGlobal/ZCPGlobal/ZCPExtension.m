//
//  ZCPExtension.m
//  ZCPKit
//
//  Created by zcp on 2019/1/14.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "ZCPExtension.h"

@implementation ZCPExtension

- (instancetype)initWithBase:(id)base {
    if (self = [super init]) {
        self.base = base;
    }
    return self;
}

@end
