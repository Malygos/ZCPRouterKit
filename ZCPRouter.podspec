Pod::Spec.new do |spec|

  spec.name          = "ZCPRouter"
  spec.version       = "0.0.1"
  spec.author        = { "朱超鹏" => "z164757979@163.com" }
  spec.license       = { :type => 'MIT', :file => 'LICENSE' }
  spec.homepage      = "https://gitlab.com/Malygos/ZCPRouter"
  spec.source        = { :git => "https://gitlab.com/Malygos/ZCPRouter.git", :tag => "#{spec.version}" }
  spec.summary       = "ZCP ZCPRouter."
  spec.description   = <<-DESC
                       ZCP ZCPRouter. pa hao fang router
                       DESC

  spec.platform      = :ios, '8.0'
  spec.ios.deployment_target = '8.0'
  spec.module_name   = 'ZCPRouter'
  spec.framework     = 'Foundation', 'UIKit'
  spec.source_files  = "ZCPRouter/ZCPRouter/**/*.{h,m}"
  spec.resources     = 'ZCPRouter/ZCPRouter/viewMap.plist'

  spec.dependency 'ZCPGlobal'
  spec.dependency 'ZCPCategory'
  spec.dependency 'Aspects', '~>1.4.1'

end
