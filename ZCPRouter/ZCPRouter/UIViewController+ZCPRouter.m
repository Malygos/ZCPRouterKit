//
//  UIViewController+ZCPRouter.m
//  ZCPKit
//
//  Created by zcp on 2019/1/7.
//  Copyright © 2019 zcp. All rights reserved.
//

#import "UIViewController+ZCPRouter.h"
#import <objc/runtime.h>
#import "ZCPGlobal.h"
#import "Aspects.h"

@protocol __ZCPNavigatorProtocol <NSObject>

@required
@property (nonatomic, weak) UIViewController<ZCPNavigatorProtocol> *formerViewController;
@property (nonatomic, weak) UIViewController<ZCPNavigatorProtocol> *latterViewController;

@optional
- (instancetype)initWithQuery:(NSDictionary *)query;

@end


@interface _UIViewControllerZCPRouterExtension : NSObject <__ZCPNavigatorProtocol>
@property (nonatomic, strong) UIViewController *viewController;
@end
@implementation _UIViewControllerZCPRouterExtension
@synthesize formerViewController = _formerViewController;
@synthesize latterViewController = _latterViewController;
@end


@interface UIViewController ()
@property (nonatomic, strong) _UIViewControllerZCPRouterExtension *zcpRouterExtension;
@end

@implementation UIViewController (ZCPRouter)

#pragma mark - __ZCPNavigatorProtocol

- (UIViewController<ZCPNavigatorProtocol> *)formerViewController {
    return [self.zcpRouterExtension formerViewController];
}
- (void)setFormerViewController:(UIViewController<ZCPNavigatorProtocol> *)formerViewController {
    return [self.zcpRouterExtension setFormerViewController:formerViewController];
}
- (UIViewController<ZCPNavigatorProtocol> *)latterViewController {
    return [self.zcpRouterExtension latterViewController];
}
- (void)setLatterViewController:(UIViewController<ZCPNavigatorProtocol> *)latterViewController {
    return [self.zcpRouterExtension setLatterViewController:latterViewController];
}
- (instancetype)initWithQuery:(NSDictionary *)query {
    return [self init];
}

#pragma mark - swizzing

+ (void)load {
    NSError * __strong error = nil;
    
    [UIViewController aspect_hookSelector:@selector(presentViewController:animated:completion:) withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo> aspectInfo) {
        UIViewController *vc    = aspectInfo.instance;
        NSArray *arguments      = aspectInfo.arguments;
        [vc zcp_presentViewController:arguments[0] animated:[arguments[1] boolValue] completion:[arguments[2] copy]];
    } error:&error];
    
    if (error) NSAssert(YES, error.localizedDescription);
    
    [UIViewController aspect_hookSelector:@selector(dismissViewControllerAnimated:completion:) withOptions:AspectPositionBefore usingBlock:^(id<AspectInfo> aspectInfo) {
        UIViewController *vc    = aspectInfo.instance;
        NSArray *arguments      = aspectInfo.arguments;
        [vc zcp_dismissViewControllerAnimated:[arguments[0] boolValue] completion:[arguments[1] copy]];
    } error:&error];
    
    if (error) NSAssert(YES, error.localizedDescription);
}

- (void)zcp_presentViewController:(UIViewController<ZCPNavigatorProtocol> *)viewControllerToPresent animated:(BOOL)flag completion:(void (^)(void))completion {
    SELF_CONFORMS_TO_PROTOCOL(@protocol(ZCPNavigatorProtocol), ^() {
        self.latterViewController = viewControllerToPresent;
        if ([viewControllerToPresent conformsToProtocol:@protocol(ZCPNavigatorProtocol)]) {
            viewControllerToPresent.formerViewController = (UIViewController <ZCPNavigatorProtocol>*)self;
        }
    });
}

- (void)zcp_dismissViewControllerAnimated:(BOOL)flag completion:(void (^)(void))completion {
    SELF_CONFORMS_TO_PROTOCOL(@protocol(ZCPNavigatorProtocol), ^() {
        self.formerViewController.latterViewController  = nil;
    });
}

#pragma mark - getters and setters

- (_UIViewControllerZCPRouterExtension *)zcpRouterExtension {
    _UIViewControllerZCPRouterExtension *extension = objc_getAssociatedObject(self, @selector(zcpRouterExtension));
    if (!extension) {
        extension = [[_UIViewControllerZCPRouterExtension alloc] init];
        extension.viewController = self;
        self.zcpRouterExtension = extension;
    }
    return extension;
}

- (void)setZcpRouterExtension:(_UIViewControllerZCPRouterExtension *)zcpRouterExtension {
    objc_setAssociatedObject(self, @selector(zcpRouterExtension), zcpRouterExtension, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
