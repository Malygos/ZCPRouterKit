//
//  UIViewController+ZCPRouter.h
//  ZCPKit
//
//  Created by zcp on 2019/1/7.
//  Copyright © 2019 zcp. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#pragma mark - UIViewController路由扩展

@interface UIViewController (ZCPRouter)

@end

#pragma mark - 导航器管理的视图控制器协议

@protocol ZCPNavigatorProtocol <NSObject>

@required
/// 前一控制器
@property (nonatomic, weak) UIViewController<ZCPNavigatorProtocol> *formerViewController;
/// 后一控制器
@property (nonatomic, weak) UIViewController<ZCPNavigatorProtocol> *latterViewController;

@optional
/// 带参初始化方法
/// @param query 参数字典
- (instancetype)initWithQuery:(NSDictionary *)query;

@end

NS_ASSUME_NONNULL_END
